package org.example.list;

import java.util.*;


/**
 * Задача - мы должны реализовать свой вариант методов интерфейса лист @link java.util.List;
 * шаблон реализации нужно посмотреть в классе @link java.util.ArrayList
 * те методы что нам нужны -  отмечены todo
 * в процессе реализации надо озакомиться с паттерном итератор
 * https://refactoring.guru/ru/design-patterns/iterator/java/example (впн)
 * так же скорее всего надо будет изучить тему generics
 * Покрыть тестами задачу
 */
public class CustomArrayList<T> implements List<T> {

    private T[] customArrayList = (T[]) new Object[]{};
    private int count = 0;

    @Override
    public int size() {
        return count;
    }

    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        if (customArrayList.length == count) {
            // Создаем новый массив увеличенного размера
            T[] customArrayListTooBig = (T[]) new Object[customArrayList.length + 1];
            // Копируем основной массив
            System.arraycopy(customArrayList, 0, customArrayListTooBig, 0, customArrayList.length);
            // Меняем ссылку основного массива на ссылку увеличенного массива
            customArrayList = customArrayListTooBig;
        }
        // Добавляем новый элемент
        customArrayList[count] = t;
        count++;
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        // Присваиваем ссылку на новый объект
        customArrayList = (T[]) new Object[1];
        count = 0;
    }

    @Override
    public T get(int index) {
        // Проверяем валидность индекса
        checkIndex(index);
        return customArrayList[index];
    }

    @Override
    public T set(int index, T element) {
        return null;
    }

    @Override
    public void add(int index, T element) {
        // Проверяем валидность индекса
        checkIndex(index);
        // Создаем новый массив увеличенного размера
        T[] customArrayListNewElement = (T[]) new Object[count + 1];
        // Формруем логику копирования основного массива (вставка в середину/конец массива и вставка в начало массива)
        if (index > 0) {
            System.arraycopy(customArrayList, 0, customArrayListNewElement, 0, index);
            System.arraycopy(customArrayList, index - 1, customArrayListNewElement, index, count - index + 1);
        } else {
            System.arraycopy(customArrayList, 0, customArrayListNewElement, 1, count);
        }
        // Меняем ссылку основного массива на ссылку увеличенного массива
        customArrayList = customArrayListNewElement;
        // Добавляем элемент
        customArrayList[index] = element;
        count++;
    }

    @Override
    public T remove(int index) {
        // Проверяем валидность индекса
        checkIndex(index);
        // Создаем новый масив меньшего размера
        T[] customArrayListWithoutDeletedElement = (T[]) new Object[count - 1];
        // Сохраняем значение удаленного элемента
        T deletedElement = customArrayList[index];
        // Формируем логику смещения элементов после удаленного
        if (index <= count) {
            for (int i = 0; i < count; i++) {
                if (i < index) {
                    customArrayListWithoutDeletedElement[i] = customArrayList[i];
                }
                if (i > index) {
                    customArrayListWithoutDeletedElement[i - 1] = customArrayList[i];
                }
            }
        }
        // Обновляем количество элементов в массиве
        count = customArrayListWithoutDeletedElement.length;
        // Меняем ссылку основного массива на ссылку уменьшенного массива
        customArrayList = customArrayListWithoutDeletedElement;
        return deletedElement;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }

    /**
     * Метод проверки индекса на валидность
     * @param index переданный индекс элемента массива
     */
    public void checkIndex(int index) {
        if (index > count || index < 0) {
            throw new IndexOutOfBoundsException("Index " + index + " out of bounds for length " + count);
        }
    }
}
