package org.example;

import org.example.list.CustomArrayList;

public class Main {
  public static void main(String[] args) {

    // Создаем тестовый объект класса
    CustomArrayList<String> array = new CustomArrayList<>();

    // Заполняем массив тестовыми данными
    for (int i = 0; i < 5; i++) {
      array.add(String.valueOf(i + 1));
    }

    // Выводим заполненный массив в консоль
    for (int i = 0; i < array.size(); i++) {
      System.out.println(array.get(i));
    }

    // Выводим в консоль результат выполнения некоторых методов
    System.out.println("\n");
    System.out.println(array.size());
    array.add(2, String.valueOf(6));
    System.out.println(array.size());
    System.out.println(array.get(5));
    System.out.println(array.remove(1));
    System.out.println("\n");

    // Выводим модифицированный массив
    for (int i = 0; i < array.size(); i++) {
      System.out.println(array.get(i));
    }
  }
}