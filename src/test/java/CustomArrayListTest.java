import org.example.list.CustomArrayList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.lang.reflect.Array;

import static org.junit.jupiter.api.Assertions.*;

public class CustomArrayListTest {

    // Создаем массив для тестов
    CustomArrayList<Object> array = new CustomArrayList<>();

    /**
     * Очистка объекта перед каждым тестом
     */
    @BeforeEach
    public void initEach() {
        array.clear();
    }

    /**
     * Позитивный тест на добавление элемента без индекса
     *
     * @param input
     */
    @ParameterizedTest(name = "{index} - argument {0}")
    @ValueSource(strings = {"", "  "})
    void stringOneElementCustomArrayListAddTestPositive(String input) {
        array.add(input);
        assertSame(array.get(0), input);
    }

    /**
     * Параметризованный тест с проверкой наращивания размерности
     *
     * @param o     тестовый набор
     * @param input массив строковых данных для теста
     */
    @ParameterizedTest(name = "{index} Добавление с превышением размера")
    @ArgumentsSource(CustomArrayListArgumentsProvider.class)
    void stringListCustomArrayListAddTestPositive(int o, Object input) {
        int length = Array.getLength(input);
        for (int i = 0; i < length; i++) {
            Object element = Array.get(input, i);
            array.add(element);
        }
        assertEquals(array.size(), length);
    }

    /**
     * Параметризованный позитивный тест на удаление элемента
     *
     * @param o     тестовый набор
     * @param input массив строковых данных для теста
     */
    @ParameterizedTest(name = "{index} Удаление одного из нескольких элементов")
    @ArgumentsSource(CustomArrayListArgumentsProvider.class)
    void stringListCustomArrayListRemoveTestPositive(int o, Object input) {
        int length = Array.getLength(input);
        for (int i = 0; i < length; i++) {
            Object element = Array.get(input, i);
            array.add(element);
        }
        array.remove(4);
        Assertions.assertAll("array",
                () -> assertEquals(array.size(), length - 1),
                () -> assertEquals(array.get(4), "sixth"));

    }

    /**
     * Позитивный тест на удаление единственного элемента объекта
     *
     * @param input элемент для удаления
     * @throws IndexOutOfBoundsException ожидаемое исключение
     */
    @ParameterizedTest(name = "{index} Удаление единственного элемента")
    @ValueSource(strings = {"", "xxx"})
    void stringElementCustomArrayListRemoveTestPositive(String input) throws IndexOutOfBoundsException {
        array.add(input);
        array.remove(0);
        Assertions.assertAll(
                () -> assertEquals(array.size(), 0),
                () -> assertThrows(IndexOutOfBoundsException.class, () -> {
                    array.get(0);
                }));

    }

    /**
     * Негативный тест на удаление элементов из пустого объекта класса
     */
    @Test
    @DisplayName("Удаление несуществующего элемента")
    void customArrayListRemoveTestNegative() {
        Assertions.assertAll(
                () -> assertEquals(array.size(), 0),
                () -> assertThrows(NegativeArraySizeException.class, () -> {
                    array.remove(0);
                }));
    }

    /**
     * Тетс на наличие элементов в пустом объекте
     */
    @Test
    @DisplayName("Проверка наполненности пустого массива")
    void emptyCustomArrayListTestPositive() {
        assertEquals(array.isEmpty(), true);
    }

    /**
     * Негативный тест на пустоту массива с элементами
     *
     * @param o     тестовый набор
     * @param input массив тестовых строковых данных
     */
    @ParameterizedTest(name = "Проверка наполненности массива c элементами")
    @ArgumentsSource(CustomArrayListArgumentsProvider.class)
    void emptyCustomArrayListTestNegative(int o, Object input) {
        int length = Array.getLength(input);
        for (int i = 0; i < length; i++) {
            Object element = Array.get(input, i);
            array.add(element);
        }
        Assertions.assertAll(
                () -> assertEquals(array.isEmpty(), false),
                () -> assertEquals(array.size(), 6));
    }

    /**
     * Позитивный тест на проверку пустоты объекта после удаления последнего элемента
     *
     * @param input значение элемента объекта для удаления
     * @throws IndexOutOfBoundsException ожидаемое исключение
     */
    @ParameterizedTest(name = "Проверка наполненности объекта после удаления последнего элемента")
    @ValueSource(strings = {""})
    void emptyCustomArrayListRemoveLastElementTestPositive(String input) throws IndexOutOfBoundsException {
        array.add(input);
        array.remove(0);
        Assertions.assertAll(
                () -> assertEquals(array.isEmpty(), true),
                () -> assertThrows(IndexOutOfBoundsException.class, () -> {
                    array.get(0);
                }));

    }

    /**
     * Проверка объекта на пустоту после вызова метода clear()
     */
    @Test
    @DisplayName("Проверка размера пустого массива")
    void emptyCustomArrayListTestSizePositive() {
        assertEquals(array.size(), 0);
    }

    /**
     * Негативнаый тест на проверку пустоты наполненного объекта
     */
    @Test
    @DisplayName("Проверка размера наполненного массива")
    void customArrayListTestSizePositive() {
        array.add("");
        Assertions.assertAll(
                () -> assertEquals(array.size(), 1),
                () -> assertEquals(array.isEmpty(), false));
    }

    /**
     * Позитивный тест на проверку размера наполненного массива после удаления единственного элемента
     */
    @Test
    @DisplayName("Проверка размера наполненного массива после удаления элемента")
    void customArrayListTestSizeAfterRemoveElementPositive() {
        array.add("");
        array.remove(0);
        Assertions.assertAll(
                () -> assertEquals(array.size(), 0),
                () -> assertEquals(array.isEmpty(), true));
    }

    /**
     * Позитивный тест на получение элемента пустого объекта
     */
    @Test
    @DisplayName("Получение элемента пустого массива")
    void customArrayListGetEmptyListTestPositive() {
        Assertions.assertAll(
                () -> assertEquals(array.get(0), null),
                () -> assertThrows(IndexOutOfBoundsException.class, () -> {
                    array.get(1);
                }),
                () -> assertEquals(array.size(), 0));
    }

    /**
     * Позитивный тест на получение элемента наполненного объекта
     *
     * @param o     тестовый набор
     * @param input массив строковых данных для проверки
     */
    @ParameterizedTest(name = "Получение элементов заполненного массива")
    @ArgumentsSource(CustomArrayListArgumentsProvider.class)
    void customArrayListGetSomeElementsTestPositive(int o, Object input) {
        int length = Array.getLength(input);
        for (int i = 0; i < length; i++) {
            Object element = Array.get(input, i);
            array.add(element);
        }
        Assertions.assertAll(
                () -> assertEquals(array.get(0), "first"),
                () -> assertEquals(array.get(5), "sixth"),
                () -> assertEquals(array.size(), 6));
    }

    /**
     * Позитивный тест на проверку пустоты массива после очистки
     */
    @Test
    @DisplayName("Проверка очистки масива")
    void customArrayListClearTestPositive() {
        array.add("");
        array.remove(0);
        Assertions.assertAll(
                () -> assertEquals(array.size(), 0),
                () -> assertEquals(array.isEmpty(), true));
    }

    /**
     * Позитивный тест на проверку добавления элемента с указанным индексом и смещением остальных элементов
     *
     * @param index индекс нового элемента
     * @param value значение нового элемента
     */
    @ParameterizedTest
    @CsvSource({"0, 10", "2, 10", "5, 10"})
    void customArrayListAddWithIndexPositive(int index, Integer value) {
        int j = 1;
        for (int i = 0; i < 5; i++) {
            array.add(j);
            j++;
        }
        array.add(index, value);
        assertEquals(array.get(index), value);
    }

    /**
     * Негативный тест на проверку добавления элемента с указанным индексом и смещением остальных элементов
     * @param index индекс нового элемента
     * @param value значение нового элемента
     */
    @ParameterizedTest
    @CsvSource({"-1, 10", "100, 10"})
    void customArrayListAddWithIndexNegative(int index, Integer value) {
        int j = 1;
        for (int i = 0; i < 5; i++) {
            array.add(j);
            j++;
        }
        Assertions.assertAll(
                () -> assertThrows(IndexOutOfBoundsException.class, () -> {
                    array.add(index, value);
                }),
                () -> assertEquals(array.size(), 5));
    }
}

